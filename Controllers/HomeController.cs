﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreMvcRollbar.Models;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using log4net;

namespace AspNetCoreMvcRollbar.Controllers
{
    public class HomeController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public IConfiguration Configuration { get; }

        public HomeController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IActionResult Index()
        {

            log.Debug("Debug logging.");
            log.Info("info-logging.- not to rollbar on TestServer config.");
            log.Warn("Warn logging - is it in Rollbar on TestServer ?? - should be.");
            log.Error("Error logging - is it in Rollbar on TestServer ?? - should be.");

            var val = Configuration.GetSection("Rollbar:Environment");
            log.Warn("Config for Rollbar Environment <in Controller> is: " + val.Value);

            throw new Exception("Testing rollbar throw");
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
